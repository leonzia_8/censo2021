package Censo;

public class Persona {
	  private int id;
	  private int dni;
	  private String nombre;
	  private String apellido;
	  private String fechaNacimiento;
	  private String sexo;
	  private String direccion;
	  private String telefono;
	  private String ocupacion;
	  private double ingresoMensual;
	  
	public String mostrarDatos() {
		String devuelvo="";
		devuelvo +="ID: " + this.getId();
		devuelvo +=", DNI: " + this.getDni();
		devuelvo +=", Nombre: " + this.getNombre();
		devuelvo +=", Apellido: " + this.getApellido();
		devuelvo +=", Fecha de Nacimiento: " + this.getFechaNacimiento() +".\n";
		devuelvo +="Sexo: " + this.getSexo();
		devuelvo +=", Direccion: " + this.getDireccion();
		devuelvo +=", Telefono: " + this.getTelefono();
		devuelvo +=", Ocupacion: " + this.getOcupacion();
		devuelvo +=", Ingreso mensual: $" + this.getIngresoMensual()+ ".\n\n";
		
		return devuelvo;
	}
	  
	  
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDni() {
		return dni;
	}
	public void setDni(int dni) {
		this.dni = dni;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}	
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	public double getIngresoMensual() {
		return ingresoMensual;
	}
	public void setIngresoMensual(double ingresoMensual) {
		this.ingresoMensual = ingresoMensual;
	}
	    

}
