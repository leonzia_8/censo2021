package Censo;
import java.sql.*;
import java.util.ArrayList;

public class PersonaCRUD {
	
	 public static void addPersona(Persona persona) throws Exception {	 
	    Connection con = AdministradorConexion.obtenerConexion();
        String query = "insert into persona (DNI, Nombre, Apellido, FechaNacimiento, Sexo, Direccion, Telefono, Ocupacion, IngresoMensual) values"
	                + " ("+ persona.getDni() +  ", '" + persona.getNombre() + "', '" +  persona.getApellido() + "', '" +persona.getFechaNacimiento() +  "', '" + persona.getSexo() + "', '" + persona.getDireccion() + "', '"  + persona.getTelefono() + "', '"+ persona.getOcupacion() + "', " + persona.getIngresoMensual()+  ")";     
	        Statement st = con.createStatement();
	        st.execute(query);
	        st.close();
	        con.close();	       
	    }
	 
	 public static ArrayList<Persona> getPersonas(int num) throws Exception {
		    ArrayList<Persona> lista = new ArrayList();
		    String listaSexo = "";
		    String query;	
		    
		    
	        Connection con = AdministradorConexion.obtenerConexion();
	       if(num==1) {
	    	    query = "SELECT * FROM persona.persona order by Apellido asc";	    	   
	       }else if(num==2) {
	    	    query = "SELECT * FROM persona.persona where Ocupacion = 'desocupado'";
	       }else if(num==3) {
	    	    query = "SELECT * FROM persona.persona where IngresoMensual < 5000";
	       }else if(num==4) {
	    	    query = "SELECT Case when Sexo in('femenino','masculino') then Sexo Else 'otros' end as Sex, Count(sexo) as cant From persona.persona GROUP BY 1";
	       }else {
	    	    query = "SELECT * from persona";
	       }
	    		   
	    		           
	        Statement st = con.createStatement();
	        ResultSet rs = st.executeQuery(query);
	        
	        if(num==4) {
	        	while (rs.next()){
	        		listaSexo += rs.getString(1) + ": ";
	        		listaSexo += rs.getString(2) + "\n";
	        	}
	        }else {
	        	 while (rs.next()){
	 	        	Persona persona = new Persona();
	 	            persona.setId(rs.getInt(1));
	 	            persona.setDni(rs.getInt(2));
	 	            persona.setNombre(rs.getString(3));
	 	            persona.setApellido(rs.getString(4));
	 	            persona.setFechaNacimiento(rs.getString(5));
	 	            persona.setSexo(rs.getString(6));
	 	            persona.setDireccion(rs.getString(7));
	 	            persona.setTelefono(rs.getString(8));
	 	            persona.setOcupacion(rs.getString(9));
	 	            persona.setIngresoMensual(rs.getDouble(10));
	 	            lista.add(persona);
	 	        } 
	        	
	        	
	        }
	        
	        
	        System.out.println(listaSexo);
	       
	
	        rs.close();
	        st.close();
	        con.close();
	        
	        return lista;
	    }
	 	 
	 
	 public static void borrarPersona(int id) throws Exception {
	        Connection con = AdministradorConexion.obtenerConexion();
	        String query = "delete from persona.persona where idPersona =" + id;

	        Statement st = con.createStatement();
	        st.execute(query);

	        st.close();
	        con.close();

	    }
	 
	 public static void mostrarPersona(int id) throws Exception {
	        Connection con = AdministradorConexion.obtenerConexion();
	        String query = "select * from persona where idPersona =" + id;

	        Statement st = con.createStatement();
	        ResultSet rs = st.executeQuery(query);
	        while (rs.next()) {
	        	System.out.println(rs.getInt(1));
	            System.out.println(rs.getInt(2));
	            System.out.println(rs.getString(3));
	            System.out.println(rs.getString(4));
	            System.out.println(rs.getString(5));
	            System.out.println(rs.getString(6));
	            System.out.println(rs.getString(7));
	            System.out.println(rs.getString(8));
	            System.out.println(rs.getString(9));
	            System.out.println(rs.getDouble(10));
	        }

	        rs.close();
	        st.close();
	        con.close();

	    }
	 
	 public static Persona obtener(int id) throws Exception {
	        Connection con = AdministradorConexion.obtenerConexion();
	        String query = "select * from persona where idPersona=" + id;
	        Persona persona = new Persona();
	        
	        Statement st = con.createStatement();
	        ResultSet rs = st.executeQuery(query);
	        while (rs.next()) {
	            persona.setId(rs.getInt(1));
	            persona.setDni(rs.getInt(2));
	            persona.setNombre(rs.getString(3));
	            persona.setApellido(rs.getString(4));
	            persona.setFechaNacimiento(rs.getString(5));
	            persona.setSexo(rs.getString(6));
	            persona.setDireccion(rs.getString(7));
	            persona.setTelefono(rs.getString(8));
	            persona.setOcupacion(rs.getString(9));
	            persona.setIngresoMensual(rs.getDouble(10));
	        }

	        rs.close();
	        st.close();
	        con.close();
	        return persona;
	    }
	
}
