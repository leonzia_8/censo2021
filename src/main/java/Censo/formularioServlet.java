package Censo;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;


@WebServlet(name = "formularioServlet", urlPatterns = {"/exportarArchivo"})
public class formularioServlet extends HttpServlet {
	
    final static Gson CONVERTIR = new Gson();
	/**
	 * 
	 */
	private static final long serialVersionUID = -4198996109153587791L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws 	ServletException, IOException{
			resp.setHeader("Access-Control-Allow-Origin", "*");
			resp.setContentType("text/html;charset=UTF-8");
	        PrintWriter out = resp.getWriter();
	        
	        String forma = req.getParameter("imprime");   
	        
	        
//	        String texto = req.getReader().readLine();
       
		try {
			 ArrayList<Persona> listado;
			 String resultado;
			 String nombreArchivo = "";	 
			 String impresionDevolucion = "";	 
			 

	        if(forma.equals("apellido")) {
				  listado = PersonaCRUD.getPersonas(1);
				  String auxArmado = "";
				  for(int i = 0; i<listado.size();i++) {
					  auxArmado += listado.get(i).mostrarDatos();
				  }
				  resultado = auxArmado;
				  nombreArchivo = "ascendenteApellido.txt";
				  impresionDevolucion = "Archivo por apellidos exportado";
	        	
	        }else if(forma.equals("mayores")) {
				  listado = PersonaCRUD.getPersonas(2);
				  String auxArmado = "";
				  for(int i = 0; i<listado.size();i++) {
					  auxArmado += listado.get(i).mostrarDatos();
				  }
				  resultado = auxArmado;
				  nombreArchivo = "mayoresdesocupados.txt";
				  impresionDevolucion = "Archivo desocupados exportado";
				        	
			}else if(forma.equals("pobre")) {
				  listado = PersonaCRUD.getPersonas(3);
				  String auxArmado = "";
				  for(int i = 0; i<listado.size();i++) {
					  auxArmado += listado.get(i).mostrarDatos();
				  }
				  resultado = auxArmado;
				  nombreArchivo = "debajoLinea.txt";
				  impresionDevolucion ="Archivo pobreza exportado";
				
			}else if(forma.equals("sexo")) {
				 listado = PersonaCRUD.getPersonas(4);
				  String auxArmado = "";
				  for(int i = 0; i<listado.size();i++) {
					  auxArmado += listado.get(i).mostrarDatos();
				  }
				  resultado = auxArmado;
				  nombreArchivo = "cantidadSexos.txt";
				  impresionDevolucion="Archivo dividido por sexos exportado";
				
			}else {
			  listado = PersonaCRUD.getPersonas(5);
			  String auxArmado = "";
			  for(int i = 0; i<listado.size();i++) {
				  auxArmado += listado.get(i).mostrarDatos();
			  }
			  resultado = auxArmado;			}
			 		 
			 
			 try {
					File fichero = new File(nombreArchivo);
					FileWriter fw= new FileWriter(fichero, true);
					fw.write(resultado);
					
					fw.flush();
					fw.close();

				  out.println(CONVERTIR.toJson(impresionDevolucion));
					
					
				}catch(IOException e){
					System.err.println(e.getMessage());
				}			 
			 
			 
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			
		
		}
	
	
	 @Override
	    protected void doOptions(HttpServletRequest req, HttpServletResponse resp)
	            throws ServletException, IOException {
	        setAccessControlHeaders(resp);
	        resp.setStatus(HttpServletResponse.SC_OK);
	    }

	    private void setAccessControlHeaders(HttpServletResponse resp) {

	        resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
	        resp.setHeader("Access-Control-Allow-Credentials", "true");
	        resp.setHeader("Access-Control-Allow-Origin", "*");
	        resp.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
	    }

 }










