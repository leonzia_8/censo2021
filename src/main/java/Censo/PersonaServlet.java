package Censo;

import com.google.gson.Gson;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

/**
 * Rest Explained
 * https://en.wikipedia.org/wiki/Representational_state_transfer
 */
@WebServlet(name = "PersonaServlet", urlPatterns = {"/PersonaServlet"})
public class PersonaServlet extends HttpServlet {
    

    final static Gson CONVERTIR = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        String forma = request.getParameter("imprime"); 
        
        try {
            String texto = request.getReader().readLine();
            ArrayList<Persona> listado;
            
            if(forma.equals("apellido")) {
				  listado = PersonaCRUD.getPersonas(1);
				 
	        }else if(forma.equals("mayores")) {
				  listado = PersonaCRUD.getPersonas(2);
				        	
			}else if(forma.equals("pobre")) {
				  listado = PersonaCRUD.getPersonas(3);
				  
				
			}else if(forma.equals("sexo")) {
				 listado = PersonaCRUD.getPersonas(4);
				 				
			}else {
				listado = PersonaCRUD.getPersonas(5);
			 
			 	}	 
			        
            String resultado = CONVERTIR.toJson(listado);
            out.println("" + resultado);

        } catch (ClassNotFoundException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (SQLException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (Exception ex) {
            out.println("Verificar:" + ex.getMessage());
        } finally {
            out.close();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String texto = request.getReader().readLine();
            String sinComillas = texto.replace("\"", "");
            
            String[] parts = sinComillas.split(",");
            String valoresFinales[] = new String[parts.length];
  
            for(int i=0;i<parts.length;i++) {
            	String[] valor = parts[i].split(":");
            	valoresFinales[i] = valor[1];         	
            }      
                     
            String[] ingresoMensual = new String[2];
            
            if(valoresFinales[8].contains("}")) {
            	ingresoMensual = valoresFinales[8].split("}");
            }


            Persona personaParametro = new Persona();

            personaParametro.setDni(Integer.parseInt(valoresFinales[0]));
            personaParametro.setNombre(valoresFinales[1]);
            personaParametro.setApellido(valoresFinales[2]);
            personaParametro.setFechaNacimiento(valoresFinales[3]);
            personaParametro.setSexo(valoresFinales[4]);
            personaParametro.setDireccion(valoresFinales[5]);
            personaParametro.setTelefono(valoresFinales[6]);
            personaParametro.setOcupacion(valoresFinales[7]);
            personaParametro.setIngresoMensual(Double.parseDouble(ingresoMensual[0]));
            System.out.println(personaParametro.getIngresoMensual());
            


            PersonaCRUD.addPersona(personaParametro);
            out.println(CONVERTIR.toJson("Datos enviados correctamente"));

        } catch (ClassNotFoundException ex) {
            out.println("Verificar: " + ex.getMessage());
        } catch (SQLException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (Exception ex) {
            out.println("Verificar:" + ex.getMessage());
        } finally {
            out.close();
        }
    }
//
//    @Override
//    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        PrintWriter out = response.getWriter();
//        try {
//            Persona personaParametro = CONVERTIR.fromJson(request.getReader(), Persona.class);
//            PersonaDao.actualizar(personaParametro);
//            out.println(CONVERTIR.toJson("OK"));
//        } catch (ClassNotFoundException ex) {
//            out.println("Verificar:" + ex.getMessage());
//        } catch (SQLException ex) {
//            out.println("Verificar:" + ex.getMessage());
//        } catch (Exception ex) {
//            out.println("Verificar:" + ex.getMessage());
//        } finally {
//            out.close();
//        }
//    }
//
    @Override
    protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      	response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
        	System.out.println("paso x aca");
        	int idPersona = Integer.parseInt(request.getParameter("id"));
        	
            
            PersonaCRUD.borrarPersona(idPersona);
            out.println(CONVERTIR.toJson("OK"));

        } catch (ClassNotFoundException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (SQLException ex) {
            out.println("Verificar:" + ex.getMessage());
        } catch (Exception ex) {
            out.println("Verificar:" + ex.getMessage());
        } finally {
            out.close();
        }
    }
    
    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        setAccessControlHeaders(resp);
        resp.setStatus(HttpServletResponse.SC_OK);
    }

    private void setAccessControlHeaders(HttpServletResponse resp) {

        resp.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        resp.setHeader("Access-Control-Allow-Credentials", "true");
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
    }

}
